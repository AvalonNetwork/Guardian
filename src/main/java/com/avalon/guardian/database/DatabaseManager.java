package com.avalon.guardian.database;

import com.avalon.guardian.Guardian;
import com.avalon.guardian.configuration.Configuration;

import lombok.Getter;

public class DatabaseManager {
	
	/**
	 * Instance
	 */
	
	@Getter private static DatabaseManager instance;
	
	/**
	 * @Constructor
	 */
	
	public DatabaseManager() {
		instance = this;
	}
	
	public void connect() { 
		Configuration c = Guardian.getConfiguration();
		new Database().connect(c.getHostname(), c.getPort(), c.getDatabase(), c.getUsername(), c.getPassword());
		
		Guardian.log("&4* &aDatabase successfully connected!");
	}
	
	public void createTables() { 
		Database.getInstance().createTable("guardian", "id MEDIUMINT PRIMARY KEY NOT NULL AUTO_INCREMENT", "username VARCHAR(32)", "auth_key VARCHAR(128)", "ip VARCHAR(32)");
		
		Guardian.log("&4* &a1 table created.");
	}
	
	public void stop() {
		Guardian.log("&4* &cDatabase connection close quietly!");
		Database.instance.closeConnection();
	}
	

}
