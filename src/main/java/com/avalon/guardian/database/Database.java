package com.avalon.guardian.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.avalon.guardian.Guardian;

import lombok.Getter;

public class Database {
	@Getter public static Database instance;
	
	private Connection connection = null;
  
	public Database() {
		instance = this;
	}
  
	public boolean connect(String host, String port, String database, String username, String password) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			this.connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
			return true;
		}
		
		catch (SQLException e) {
			Guardian.log("&cCan't connect to the server.");
			e.printStackTrace();
		}
		
		catch (ClassNotFoundException e) {
			System.out.println("JDBC Driver not found!");
		}
		
		return false;
	}
  
  public boolean isConnected()
  {
    if (this.connection != null) {
      return true;
    }
    return false;
  }
  
  public void closeConnection()
  {
    if (this.connection == null) {
      try
      {
        this.connection.close();
      }
      catch (SQLException e)
      {
        System.out.println("Couldn't close connection");
      }
    }
  }
  
  public boolean execute(String sql)
  {
    boolean st = false;
    try
    {
      Statement statement = this.connection.createStatement();
      st = statement.execute(sql);
      statement.close();
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    return st;
  }
  
  public ResultSet executeQuery(String sql)
  {
    PreparedStatement statement = null;
    ResultSet rs = null;
    try
    {
      statement = this.connection.prepareStatement(sql);
      rs = statement.executeQuery();
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    return rs;
  }
  
  public void closeQuietly(ResultSet rs)
  {
    PreparedStatement ps = null;
    try
    {
      ps = (PreparedStatement)rs.getStatement();
      rs.close();
      rs = null;
      ps.close();
      ps = null;
    }
    catch (SQLException e)
    {
      e.printStackTrace();
      if (rs != null) {
        try
        {
          rs.close();
        }
        catch (SQLException localSQLException1) {}
      }
      if (ps != null) {
        try
        {
          rs.close();
        }
        catch (SQLException localSQLException2) {}
      }
    }
    finally
    {
      if (rs != null) {
        try
        {
          rs.close();
        }
        catch (SQLException localSQLException3) {}
      }
      if (ps != null) {
        try
        {
          rs.close();
        }
        catch (SQLException localSQLException4) {}
      }
    }
  }
  
  public int executeUpdate(String sql)
  {
    int st = 0;
    try
    {
      Statement statement = this.connection.createStatement();
      st = statement.executeUpdate(sql);
      statement.close();
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    return st;
  }
  
  public void executeBatch(String[] statements, int batchSize)
  {
    int count = 0;
    
    Statement statement = null;
    try
    {
      statement = this.connection.createStatement();
      for (String query : statements)
      {
        statement.addBatch(query);
        if ((batchSize != 0) && 
          (count % batchSize == 0)) {
          statement.executeBatch();
        }
        count++;
      }
      statement.executeBatch();
      statement.close();
      statement = null;
    }
    catch (SQLException e)
    {
      e.printStackTrace();
      if (statement != null) {
        try
        {
          statement.close();
        }
        catch (SQLException localSQLException1) {}
      }
    }
    finally
    {
      if (statement != null) {
        try
        {
          statement.close();
        }
        catch (SQLException localSQLException2) {}
      }
    }
  }
  
  public void createTable(String tablename, String... values)
  {
    String stmt = "CREATE TABLE IF NOT EXISTS " + tablename + "(";
    for (int i = 0; i < values.length; i++) {
      if (i == values.length - 1) {
        stmt = stmt.concat(values[i]);
      } else {
        stmt = stmt.concat(values[i] + ", ");
      }
    }
    stmt = stmt.concat(");");
    try
    {
      Statement statement = this.connection.createStatement();
      statement.executeUpdate(stmt);
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
  }
  
  public void deleteTable(String tablename)
  {
    String sql = "DROP TABLE IF EXISTS " + tablename;
    try
    {
      Statement statement = this.connection.createStatement();
      statement.executeUpdate(sql);
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
  }
  
  public void resetTable(String tablename, String[] values)
  {
    deleteTable(tablename);
    createTable(tablename, values);
  }
  
  public void insertInto(String table, String[] columns, Object[] values)
  {
    String statement = "INSERT INTO " + table;
    
    String c = "(";
    for (int i = 0; i < columns.length; i++) {
      if (i == columns.length - 1) {
        c = c + columns[i];
      } else {
        c = c + columns[i] + ",";
      }
    }
    c = c + ")";
    
    String v = "(";
    for (int i = 0; i < values.length; i++) {
      if (i == columns.length - 1)
      {
        if ((values[i] instanceof String)) {
          v = v + "'" + values[i] + "'";
        } else {
          v = v + values[i];
        }
      }
      else if ((values[i] instanceof String)) {
        v = v + "'" + values[i] + "', ";
      } else {
        v = v + values[i] + ", ";
      }
    }
    v = v + ")";
    
    statement = statement + c + " VALUES" + v + " ON DUPLICATE KEY UPDATE ";
    for (int i = 0; i < columns.length; i++)
    {
      statement = statement + columns[i] + "=";
      if (i == columns.length - 1)
      {
        if ((values[i] instanceof String)) {
          statement = statement + "'" + values[i] + "'";
        } else {
          statement = statement + values[i];
        }
      }
      else if ((values[i] instanceof String)) {
        statement = statement + "'" + values[i] + "', ";
      } else {
        statement = statement + values[i] + ", ";
      }
    }
    statement = statement + ";";
    executeUpdate(statement);
  }
  
  public void insertIntoWithoutPrimaryKey(String table, String[] columns, Object[] values)
  {
    String statement = "INSERT INTO " + table;
    
    String c = "(";
    for (int i = 0; i < columns.length; i++) {
      if (i == columns.length - 1) {
        c = c + columns[i];
      } else {
        c = c + columns[i] + ",";
      }
    }
    c = c + ")";
    
    String v = "(";
    for (int i = 0; i < values.length; i++) {
      if (i == columns.length - 1)
      {
        if ((values[i] instanceof String)) {
          v = v + "'" + values[i] + "'";
        } else {
          v = v + values[i];
        }
      }
      else if ((values[i] instanceof String)) {
        v = v + "'" + values[i] + "', ";
      } else {
        v = v + values[i] + ", ";
      }
    }
    v = v + ")";
    
    statement = statement + c + " VALUES" + v + " ON DUPLICATE KEY UPDATE ";
    for (int i = 1; i < columns.length; i++)
    {
      statement = statement + columns[i] + "=";
      if (i == columns.length - 1)
      {
        if ((values[i] instanceof String)) {
          statement = statement + "'" + values[i] + "'";
        } else {
          statement = statement + values[i];
        }
      }
      else if ((values[i] instanceof String)) {
        statement = statement + "'" + values[i] + "', ";
      } else {
        statement = statement + values[i] + ", ";
      }
    }
    statement = statement + ";";
    
    executeUpdate(statement);
  }
  
  public ResultSet getRowByColumn(String table, String column, String value)
  {
    ResultSet rs = executeQuery("SELECT * FROM " + table + " WHERE " + column + " = " + value);
    
    return rs;
  }
  
  public String getValueFromRow(String table, ResultSet set, String column)
  {
    try
    {
      return set.getString(column);
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    return null;
  }
  
  public int getRowCount(String table)
  {
    ResultSet rs = executeQuery("SELECT * FROM " + table);
    int count = 0;
    try
    {
      while (rs.next()) {
        count++;
      }
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    return count;
  }
  
  public Connection getConnection()
  {
    return this.connection;
  }
}
 