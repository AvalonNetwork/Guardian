package com.avalon.guardian.configuration;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Configuration {
	
	/**
	 * Credentials 
	 */
	
	private String hostname = "localhost";
	private String port = "3306";
	private String database = "lobby";
	private String username = "lobby";
	private String password = "lobby";

}
