package com.avalon.guardian.configuration;

import java.io.File;

import com.avalon.guardian.Guardian;
import com.avalon.guardian.utils.DiscUtil;

import lombok.Getter;
import net.minecraft.util.com.google.common.reflect.TypeToken;

public class ConfigurationManager {
	
	/**
	 * Instance of this classes.
	 */
	
	@Getter private static ConfigurationManager instance;
	
	/**
	 * {@linkplain Configuration} object
	 */
	
	private Configuration configuration;
			
	/**
	 * TypeToken of the Configuration (Needed by GSON)
	 */

	private TypeToken<Configuration> type = new TypeToken<Configuration>() { private static final long serialVersionUID = 1L;};
	
	/**
	 * @Constructor 
	 */
	 
	public ConfigurationManager() {
		instance = this;
	}
	
	/**
	 * Return the File where the {@linkplain Configuration}
	 * @return Configuration {@linkplain File} 
	 */
	
	public File getConfigurationFile() {
		return new File(Guardian.getInstance().getDataFolder(), "configuration.json");
	}
	
	/**
	 * Return the {@linkplain Configuration} object.
	 * @return {@linkplain Configuration} object.
	 */
	
	public Configuration getConfiguration() {
		if (this.configuration == null) 
			return this.configuration = new Configuration();
		
		return this.configuration;
	}
	
	/**
	 * Load {@linkplain Configuration}
	 */
	
	public void load() {
		String content = DiscUtil.readCatch(this.getConfigurationFile());
		if (content == null) 
			return;

		try {
			this.configuration = Guardian.getInstance().getGson().fromJson(content, type.getType());
		} catch (Exception ex) {
		}
	}
	
	/**
	 * Reload {@linkplain Configuration}
	 * only reading the File, not saving him!
	 */
	
	public void reload() {
		this.load();
	}
	
	/**
	 * Save {@linkplain Configuration}
	 */
	
	public void save() {
		DiscUtil.writeCatch(this.getConfigurationFile(), Guardian.getInstance().getGson().toJson(this.configuration), true);
	}

}
