package com.avalon.guardian.command.commands;

import org.bukkit.entity.Player;

import com.avalon.guardian.command.Args;
import com.avalon.guardian.command.Command;
import com.avalon.guardian.managers.Manager;

public class PlayerCommands {
	
	@Command(name = "guard.activate",
			aliases = {"guardian.activate"},
			permission = "guard.player",
			usage = "/guardian activate",
			inGameOnly = true)
	public void GuardianActivate(Args args) {
		Manager.getInstance().create(args.getPlayer());
	}
	
	@Command(name = "guard.auth",
			aliases = {"guardian.auth"},
			permission = "guard.player",
			usage = "/guardian auth <code>",
			inGameOnly = true)
	public void GuardianAuthentificate(Args args) {
		if(args.length() != 1) {
			args.sendUsage();
			return;
		}
		
		Player player = args.getPlayer();
	
		if(Manager.getInstance().isLocked(player.getName())) 
			Manager.getInstance().check(player, Integer.parseInt(args.getArgs(0)));
	}
	
	

}
