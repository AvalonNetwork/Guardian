package com.avalon.guardian.listeners;

import java.util.concurrent.TimeUnit;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import com.avalon.guardian.managers.Manager;
import com.avalon.guardian.utils.Utils;

public class PlayerListeners implements Listener {
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void PlayerJoinEvent(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		String name   = player.getName();
		
		if(Manager.getInstance().getSecurized().containsKey(name)) {
			Manager.getInstance().lock(name);
			Manager.getInstance().getTimer().put(name, System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(1L));
			player.sendMessage(Utils.color("&8[&cGuardian&8] &cMerci de rentrer le code de s�curit�! (1 minute restante.) /guard auth <code>"));
			return;
		}
		
		player.sendMessage(Utils.color("&8[&cGuardian&8] &eVotre compte n'utilise pas Guardian! Pour activer guardian fait /guardian activate."));
	}
	
	/**
	 * Blocked some Bukkit Events,
	 *  like Chat, Moove, Break, Place, Command (guard auth is not impacted :d)
	 */
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void AsyncPlayerChatEvent(AsyncPlayerChatEvent event) { if(Manager.getInstance().isLocked(event.getPlayer().getName())) event.setCancelled(true); }

	@EventHandler(priority = EventPriority.MONITOR)
	public void PlayerMoveEvent(PlayerMoveEvent event) { if(Manager.getInstance().isLocked(event.getPlayer().getName())) event.setCancelled(true); }
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void BlockBreakEvent(BlockBreakEvent event) { if(Manager.getInstance().isLocked(event.getPlayer().getName())) event.setCancelled(true); }
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void BlockPlaceEvent(BlockPlaceEvent event) { if(Manager.getInstance().isLocked(event.getPlayer().getName())) event.setCancelled(true); }
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void PlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) { 
		if(Manager.getInstance().isLocked(event.getPlayer().getName())) {
			if(event.getMessage().startsWith("/guardian auth") || event.getMessage().startsWith("/guard auth")) 
				return;
			
			
			event.getPlayer().sendMessage(Utils.color("&8[&cGuardian&8] &cMerci de rentrer le code de s�curit�!"));
			event.setCancelled(true);
			return;
		}
	}
	
}
