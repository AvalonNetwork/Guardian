package com.avalon.guardian;

import java.lang.reflect.Modifier;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.avalon.guardian.command.CommandFramework;
import com.avalon.guardian.command.commands.AdminCommands;
import com.avalon.guardian.command.commands.PlayerCommands;
import com.avalon.guardian.configuration.Configuration;
import com.avalon.guardian.configuration.ConfigurationManager;
import com.avalon.guardian.database.DatabaseManager;
import com.avalon.guardian.listeners.PlayerListeners;
import com.avalon.guardian.managers.Manager;

import lombok.Getter;
import net.minecraft.util.com.google.gson.Gson;
import net.minecraft.util.com.google.gson.GsonBuilder;

// TODO: Si player deja login avant 5m il n'as pas pb de se co 

/**
 * Guardian - 1.0.0
 *  Guardian is a Spigot 1.7.X plugin, this plugin force {@link Player} to use a TOTP (Google Authentificator for iOS / Android and Windows Phone)
 *   if {@link Player} doesn't want to use we're not responsable of any HACK. GAuthentificator work only with TOTP Client.
 *   When the {@link Player} join the {@link Server} if him has activated GAuth and link with a TOTP Client him has to do 
 *   /guard connect <code> (<code> is like 897985 or 126458 only 6 number maybe is be separated in TOTP Client like 897 632)
 *    On do that command, {@link Player} can connect to {@link Server} without be kicked after 45s.
 *    
 *   PS: On ALL login if user has activated Guardian, user need to do /guard connect <code>, after 3 bad retry {@link Player} is temporary banned (2 hours)
 *   of server.
 *   
 * @author Neziaa
 * @version 1.0.0
 * @since 12.03.2018
 *
 */

public class Guardian extends JavaPlugin {
	
	/**
	 * INSTANCE
	 */
	
	@Getter private static Guardian instance;
	
	/**
	 * GSON
	 */
	
	@Getter private Gson gson;
	
	/**
	 * Configuration's
	 * 	ConfigurationManager, is the object who's Managing the Configuration,
	 * 	 loading him, reloading him and save him.
	 *  
	 *  Configuration, is the object who's contains the Configuration of the Server
	 *   and the Plugin's configuration, like Redis credentials.
	 *   doesn't forget to load her before return her.
	 *  
	 */
	
	@Getter private ConfigurationManager configurationManager;
	@Getter private static Configuration configuration;
	
	/**
	 * Database
	 */
	
	@Getter private DatabaseManager databaseManager;
	
	/**
	 * Manager
	 */
	
	@Getter private Manager manager;
	
	/**
	 * Commands
	 */
	
	@Getter private CommandFramework framework;
	
	/**
	 * TIMER VARIABLES
	 */
	
	private Long started;
	
	@Override
	public void onEnable() {
		/**
		 * PRE
		 */
		
		this.started = System.currentTimeMillis();
		log("-----------------------------");
		log("&4* &cStarting Guardian - " + this.getDescription().getVersion());
		
		/**
		 * INSTANCING
		 */
		
		instance = this;
		this.getDataFolder().mkdirs();
		this.gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().serializeNulls().excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.VOLATILE).create();
		
		log("&4* &aGSON successfully load!");
		
		/**
		 * CONFIGURATION
		 */
		
		this.configurationManager = new ConfigurationManager();
		configurationManager.load();
		configuration = this.configurationManager.getConfiguration();
		
		log("&4* &aConfiguration successfully load!");
		
		/**
		 * DATABASE
		 */
		
		this.databaseManager = new DatabaseManager();
		databaseManager.connect();
		databaseManager.createTables();
		
		log("&4* &aDatabase successfully initialised.");
		
		/**
		 * MANAGER?
		 */
		
		this.manager = new Manager();
		manager.load();
		
		log("&4* &aManager loaded.");
		
		/**
		 * Commands
		 */
		
		this.framework = new CommandFramework(this);
		framework.registerCommands(new PlayerCommands());
		framework.registerCommands(new AdminCommands());

		log("&4* &aCommand successfully registered.");
		
		/**
		 * Listeners
		 */
		
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerListeners(), this);
		
		/**
		 * POST
		 */
		
		log("&4* &aSuccessfully started in &3" + (started - System.currentTimeMillis()) + " &7ms.");
		log("&4* &aGuardian - " + this.getDescription().getVersion());
		log("&4* &cPowered by Avalon - https://avalon-network.net");
		log("-----------------------------");
	}
	
	
	/**
	 * Log a message into CONSOLE
	 * 
	 * @param 
	 * 		message	to log.
	 * 
	 */
	
	public static void log(String message) {
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&cGuardian&8] &7" + message));
	}
	
	@Override
	public void onDisable() {
		this.configurationManager.save();
	}

}
