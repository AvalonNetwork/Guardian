package com.avalon.guardian.managers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import com.avalon.guardian.Guardian;
import com.avalon.guardian.database.Database;
import com.avalon.guardian.utils.Utils;
import com.google.common.collect.Lists;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;

import lombok.Getter;

public class Manager {
	
	/**
	 * Instance
	 */
	
	@Getter private static Manager instance; 
	
	/**
	 * List
	 */
	
	@Getter private List<String> locked = Lists.newArrayList();
	@Getter private Map<String, Session> securized = new HashMap<String, Session>();
	
	@Getter private Map<String, Long> timer = new HashMap<String, Long>();
	
	/**
	 * Manager
	 */
	
	@Getter private BukkitTask asyncTask;
	
	/**
	 * @Constructor 
	 */
	
	public Manager() {
		instance = this;
	}
	
	public void create(Player player) {
		GoogleAuthenticator gAuth = new GoogleAuthenticator();
        GoogleAuthenticatorKey key = gAuth.createCredentials();
        
        Database.instance.insertInto("guardian",
        		new String[] { "id", "username", "auth_key", "ip"},
				new Object[] { null, player.getName(), key.getKey(), player.getAddress().getAddress().getHostAddress()}
        );
        
        Session session = new Session(player, key.getKey());
        this.securized.put(player.getName(), session);
        
        player.sendMessage(Utils.color("&8[&cGuardian&8] &eVous venez d'activer Guardian! Compte d�sormais sous protection!"));
        player.sendMessage(Utils.color("&8[&cGuardian&8] &cVotre cl� Guardian est " + key.getKey() + " rentrer la dans votre application TOTP (Google Authentificator)"));
        player.sendMessage(Utils.color("&8[&cGuardian&8] &eVous devez d�sormais faire /guardian auth <code> lors de votre connexion. "));
	}
	
	public void load() {
		ResultSet query = Database.instance.executeQuery("SELECT * FROM guardian");
		
		try {
			while(query.next()) {
				
				String username = query.getString("username");
				String key = query.getString("auth_key");
				String ip  = query.getString("ip");
				
				Session session = new Session(username, key, ip);
				this.securized.put(username, session);
				
			}
		} 
		
		catch (SQLException e) { Guardian.log("Problems while loading SESSION!"); }
	}
	
	@SuppressWarnings("deprecation")
	public void run() {
		this.asyncTask = Bukkit.getScheduler().runTaskTimer(Guardian.getInstance(), new BukkitRunnable() {
			
			public void run() {
				
				for(String str : locked) {
					Player player = Bukkit.getPlayer(str);
					
					player.sendMessage(Utils.color("&8[&cGuardian&8] &7Merci de rentrer le code de s�curit�!"));
					
					if(getTimer().get(player.getName()) <= System.currentTimeMillis()) {
						player.kickPlayer("�8[�cGuardian�8] �cVous avez mis trop de temps pour vous connecter.");
					}
				}
			}
		}, 60L, 60L);
	}
	
	public void 	lock(String player) { this.locked.add(player); }
	public boolean 	isLocked(String player) { return this.locked.contains(player); }
	public void 	unlock(String player) { this.locked.remove(player); }
	
	/**--------------------------------------
	 * 
	 * 	   Google Authentificator CHECK
	 * 
	 --------------------------------------*/
	
	
    public boolean check(Player player, int code) {
        String secretkey = this.securized.get(player.getName()).getKey();

        GoogleAuthenticator gAuth = new GoogleAuthenticator();
        boolean codeisvalid = gAuth.authorize(secretkey, code);

        if (codeisvalid) {
            locked.remove(player.getName());
            player.sendMessage(Utils.color("&8[&cGuardian&8] &aBonjour, " + player.getName() + " vous �tes d�sormais authentifi� avec certitude! Bon jeu!"));
            return codeisvalid;
        }
        
        player.sendMessage(Utils.color("&8[&cGuardian&8] &cCode invalide! Il a surement expirer ou est mauvais."));

        return codeisvalid;
    }
}
