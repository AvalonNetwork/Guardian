package com.avalon.guardian.managers;

import org.bukkit.entity.Player;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor
public class Session {
	
	private String username;
	private String key;
	private String ip;
	
	public Session(Player player, String key) {
		this.username = player.getName();
		this.key = key;
		this.ip = player.getAddress().getAddress().getHostName();
	}

}
